﻿# NodejsCAddon


## Setting up ( USING: Command Prompt Administrator )
### This ONLY NEEDS TO BE RAN ONCE
> powershell
```
PS> 
```
> PS>Start-Process powershell -Verb runAs
This is to run PowerShell as administrator...

Depending on your computer's enviroment, you may need to also self-sign the script...
> PS>Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass

Then you will be allowed to correctly link the LATEST VISUAL STUDIOS BUILD TOOLS, to `node-gyp` such that there are no errors when compiling the
C/C++ shim for the node.js application.

### COMPILE / RECOMPILE
The editable file, `build-addOn.bat`, will need to be ran anytime the C/C++ code has changed for this project! 