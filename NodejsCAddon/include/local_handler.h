#if !defined(__LOCAL_HANDLER__)
#define __LOCAL_HANDLER__
#include <iostream>
#include <napi.h>
using namespace std;

namespace example {
    string hello();
    Napi::String HelloWrapped(const Napi::CallbackInfo& info);
    Napi::Object Init(Napi::Env env, Napi::Object exports);
}

#endif // __LOCAL_HANDLER__