#include <napi.h>
#include <include/local_handler.h>
 
 Napi::Object InitAll(Napi::Env env, Napi::Object exports) {
  return example::Init(env, exports);
 }
 
NODE_API_MODULE(abiCppLocalClient, InitAll)