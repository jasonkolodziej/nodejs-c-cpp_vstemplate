#include "include/local_handler.h"

string example::hello(){
 return "ugh";
}

Napi::String example::HelloWrapped(const Napi::CallbackInfo& info) 
{
  Napi::Env env = info.Env();
  Napi::String returnValue = Napi::String::New(env, example::hello());
  
  return returnValue;
}

Napi::Object example::Init(Napi::Env env, Napi::Object exports) 
{
  exports.Set("hello", Napi::Function::New(env, example::HelloWrapped));
 
  return exports;
}