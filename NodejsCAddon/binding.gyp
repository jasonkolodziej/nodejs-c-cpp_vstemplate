{
 #   "variables": {
 
 #"dll_files" : [
  #  '<(module_root_dir)/lib/BIOSIntf.dll'
  #]
 #},
    "targets": [
        {
        "target_name": "nodeAddon",
        "cflags!": [ "-fno-exceptions" ],
        "cflags_cc!": [ "-fno-exceptions" ],
        "sources": [
            "<(module_root_dir)/src/main.cpp",
            "<(module_root_dir)/src/local_handler.cpp"
        ],
        'include_dirs': [
            "<!@(node -p \"require('node-addon-api').include\")",
               "<(module_root_dir)",
               ],
        'libraries': [],
        'dependencies': [
            "<!(node -p \"require('node-addon-api').gyp\")"
        ],
        'defines': [ 'NAPI_DISABLE_CPP_EXCEPTIONS' ]
    }
]
# #     "dll_files" : [
#     './lib/BIOSIntf.dll'
#   ]
}